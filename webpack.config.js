const path = require('path');

module.exports = (test) => {
  return {
    target: 'node',
    mode: 'development',
    entry: './src/Interpreter/index.js',
    output: {
      path: path.resolve(__dirname, 'bin'),
      filename: 'bfc',
      libraryTarget: 'umd',
      library: 'bfc'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        }
      ]
    }
  };
};
