import Interpreter from './';

const DEFAULT_MEMSIZE = 30000;
const MEMSIZE = 100;

describe('Interpreter initialization', () => {
  it('should set a pointer to the first memory cell', () => {
    const bf = new Interpreter({});
    expect(bf._pointer).toBe(0);
  });

  it('should set a default memory size', () => {
    const bf = new Interpreter({});
    expect(bf._memory.length).toBe(DEFAULT_MEMSIZE);
  });

  it('should allow configuration of memsize', () => {
    const bf = new Interpreter({ MEMSIZE });
    expect(bf._memory.length).toBe(MEMSIZE);
  });

  it('should create an array of zeros', () => {
    const bf = new Interpreter({ MEMSIZE });
    for (let i = 0; i < MEMSIZE; i++) {
      expect(bf._memory[i]).toBe(0);
    }
  });
});

describe('Running code', () => {
  it('should clean source code before running', () => {
    const bf = new Interpreter({ MEMSIZE });
    const cleaned = bf._clean(
      `some +characters -that +shou[ldn't. <be her]e, ++>+`
    );
    expect(cleaned).toBe('+-+[.<],++>+');
  });

  it('should be able to run code with comments and whitespace', () => {
    const bf = new Interpreter({ MEMSIZE });
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    expect(bf._memory[1]).toBe(0);
    expect(bf._memory[2]).toBe(0);

    bf.run(
      '+  + there +[ are a bunch >+[ of junk>+< characters-]< randomly inserted -  into this] source code'
    );

    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    expect(bf._memory[1]).toBe(0);
    expect(bf._memory[2]).toBe(3);
  });

  it.only('should kill a long-running program', () => {
    const bf = new Interpreter({ MEMSIZE, executionLimit: 5 });
    expect(() => bf.run('++++++')).toThrow();
  });
});

describe('Visualization', () => {
  const options = {
    MEMSIZE
  };

  it('should print the default state of the interpreter', () => {
    const bf = new Interpreter(options);
    expect(bf.visualize(3)).toBe('[0][0][0]\n ^');
  });

  it('should move the pointer visual properly', () => {
    const bf = new Interpreter(options);
    bf.run('>>>');
    expect(bf.visualize(4)).toBe('[0][0][0][0]\n          ^');
  });
});

describe('Brainfuck pointer manipulation', () => {
  const options = {
    MEMSIZE
  };
  let bf = null;

  beforeEach(() => {
    bf = new Interpreter(options);
  });

  it('should increment the pointer with >', () => {
    expect(bf._pointer).toBe(0);
    bf.run('>');
    expect(bf._pointer).toBe(1);
  });

  it('should increment the pointer twice with >>', () => {
    expect(bf._pointer).toBe(0);
    bf.run('>>');
    expect(bf._pointer).toBe(2);
  });

  it('should throw an exception if the pointer goes beyond MEMSIZE - 1', () => {
    bf._pointer = MEMSIZE - 1;

    try {
      bf.run('>');
      expect(false).toBe(true);
    } catch (e) {
      expect(e.message).toBe('Pointer overflow exception');
    }
  });

  it('should throw an exception if the pointer goes negative', () => {
    expect(bf._pointer).toBe(0);

    try {
      bf.run('<');
      expect(false).toBe(true);
    } catch (e) {
      expect(e.message).toBe('Pointer out of range exception');
    }
  });

  it('should decrement the pointer with <', () => {
    bf._pointer = 1;
    bf.run('<');
    expect(bf._pointer).toBe(0);
  });

  it('should decrement the pointer twice with <<', () => {
    bf._pointer = 2;
    bf.run('<<');
    expect(bf._pointer).toBe(0);
  });
});

describe('Brainfuck cell manipulation', () => {
  const options = {
    MEMSIZE
  };
  let bf = null;

  beforeEach(() => {
    bf = new Interpreter(options);
  });

  it('should increment the current cell value with +', () => {
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    bf.run('+');
    expect(bf._memory[0]).toBe(1);
  });

  it('should increment the current cell value twice with ++', () => {
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    bf.run('++');
    expect(bf._memory[0]).toBe(2);
  });

  it('should decrement the current cell value with -', () => {
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    bf.run('-');
    expect(bf._memory[0]).toBe(-1);
  });

  it('should decrement the current cell value twice with --', () => {
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    bf.run('--');
    expect(bf._memory[0]).toBe(-2);
  });

  it('should be able to combine operations', () => {
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    expect(bf._memory[1]).toBe(0);
    expect(bf._memory[2]).toBe(0);
    expect(bf._memory[3]).toBe(0);

    bf.run('+>+>+>+<<<->->->-');

    expect(bf._pointer).toBe(3);
    expect(bf._memory[0]).toBe(0);
    expect(bf._memory[1]).toBe(0);
    expect(bf._memory[2]).toBe(0);
    expect(bf._memory[3]).toBe(0);
  });
});

describe('Brainfuck loops', () => {
  const options = {
    MEMSIZE
  };
  let bf = null;

  beforeEach(() => {
    bf = new Interpreter(options);
  });

  it('should repeat the contents of a loop until the current cell is 0', () => {
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    expect(bf._memory[1]).toBe(0);

    bf.run('+++[>+<-]');

    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    expect(bf._memory[1]).toBe(3);
  });

  it('should handle nested loops', () => {
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    expect(bf._memory[1]).toBe(0);
    expect(bf._memory[2]).toBe(0);

    bf.run('+++[>+[>+<-]<-]');

    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    expect(bf._memory[1]).toBe(0);
    expect(bf._memory[2]).toBe(3);
  });
});

describe('Brainfuck IO', () => {
  const options = {
    MEMSIZE
  };
  let bf = null;

  beforeEach(() => {
    bf = new Interpreter(options);
  });

  it('should output the current cell as ASCII with .', () => {
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);

    bf.run('+++++++++++++++++++++++++++++++++.');

    expect(bf._memory[0]).toBe(33);
    expect(bf.output).toBe('!');
  });

  it('should print Hello World!', () => {
    bf.run(
      '++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.'
    );

    expect(bf.output).toBe('Hello World!');
  });

  it('should accept 1 character of input with ,', () => {
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);

    bf.input = 'A';
    bf.run(',');

    expect(bf._memory[0]).toBe(65);
  });

  it('should not accept more than 1 character of input with ,', () => {
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);

    bf.input = 'ABC';
    bf.run(',');

    expect(bf._memory[0]).toBe(65);
  });

  it('should accept two characters of input with ,,', () => {
    expect(bf._pointer).toBe(0);
    expect(bf._memory[0]).toBe(0);
    expect(bf._memory[1]).toBe(0);

    bf.input = 'ABC';
    bf.run(',>,');

    expect(bf._memory[0]).toBe(65);
    expect(bf._memory[1]).toBe(66);
  });
});
