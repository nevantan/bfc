import readline from 'readline-sync';

const DEFAULT = {
  MEMSIZE: 30000,
  logOutput: false,
  automated: false,
  executionLimit: 0
};

export default class Interpreter {
  constructor(options = DEFAULT) {
    this._options = {
      ...DEFAULT,
      ...options
    };

    this._pointer = 0;
    this._memory = [];

    for (let i = 0; i < this._options.MEMSIZE; i++) {
      this._memory[i] = 0;
    }

    this._output = '';
    this._input = '';

    this._instructions = 0;
  }

  run(src, i = 0) {
    src = this._clean(src);
    for (; i < src.length; i++) {
      this._instructions++;
      if (
        this._options.executionLimit > 0 &&
        this._instructions > this._options.executionLimit
      ) {
        throw new Error('Program exceeded the execution limit');
      }

      switch (src[i]) {
        case '>':
          this._pointer++;
          if (this._pointer >= this._options.MEMSIZE)
            throw new Error('Pointer overflow exception');
          break;
        case '<':
          this._pointer--;
          if (this._pointer < 0)
            throw new Error('Pointer out of range exception');
          break;
        case '+':
          this._memory[this._pointer]++;
          break;
        case '-':
          this._memory[this._pointer]--;
          break;
        case '[':
          while (this.run(src, i + 1));

          let skips = 0;
          while (true) {
            if (src[i + 1] === '[') skips++;
            if (src[i + 1] === ']' && skips === 0) break;
            else if (src[i + 1] === ']') skips--;
            i++;
          }
          i++;
          break;
        case ']':
          if (this._memory[this._pointer] == 0) return false;
          else return true;
        case '.':
          if (this._options.logOutput) {
            console.log(String.fromCharCode(this._memory[this._pointer]));
          } else {
            this._output += String.fromCharCode(this._memory[this._pointer]);
          }
          break;
        case ',':
          if (this._input === '') {
            if (automated) throw new Error('No more input available');
            this.input = readline.question('> ');
          }
          this._memory[this._pointer] = this.input;
          break;
      }
    }

    return true;
  }

  visualize(cells) {
    let result = '';
    for (let i = 0; i < cells; i++) {
      result += `[${this._memory[i]}]`;
    }
    result += '\n ';
    for (let i = 0; i < this._pointer; i++) {
      result += '   ';
    }
    result += '^';
    return result;
  }

  get output() {
    return this._output;
  }

  get input() {
    if (this._input.length === 0) return '';
    const char = this._input[0];
    this._input = this._input.slice(1);
    return char.charCodeAt(0);
  }

  set input(val) {
    this._input += val;
  }

  _clean(src) {
    return src
      .split('')
      .filter((c) => ['+', '-', '<', '>', '[', ']', '.', ','].includes(c))
      .join('');
  }
}

// 300
