import fs from 'fs';

import Interpreter from './Interpreter';

const bf = new Interpreter({
  logOutput: false
});

fs.readFile(process.argv[2], (err, src) => {
  if (err) console.error(err);
  bf.run(src.toString());
  console.log(bf.output);
});
